# La magie du dé
HTML/CSS/JS Integration

## Requirements
- [Node.js](http://nodejs.org)
- [Grunt](http://gruntjs.com/) : npm install -g grunt-cli

## Includes

### Tasks
- [Jade](http://jade-lang.com/) for Html templating
- [PostCSS](https://github.com/postcss/postcss) and [cssnext](http://cssnext.io/) for variables, imports, auto-prefixing (...) in css files.
- Local server and live reload.
- Html and Css validation

## Getting started
Setup ```npm install```

Run ```grunt```

## Version

1.0.0
- Creation

@JeromeRaffin